class PagesController < ApplicationController
  def about
  end

  def courses
  end

  def locations
  end

  def contact
  end

  def home
  end
end
